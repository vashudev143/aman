import MeetupDetail from "../../components/meetups/MeetupDetail";
import Head from "next/head";
import { MongoClient, ObjectId } from "mongodb";
const MeetupDetails = (props) => {
  return (
    <>
    <Head>
      <title>View</title>
      <meta 
      name="description"
      content="Watch react meetups"
      />
    </Head>
    <MeetupDetail
      img={props.meetup?.image}
      address={props.meetup?.address}
      title={props.meetup?.title}
      />
</>
  );
};
export const getStaticPaths = async () => {
  const client = await MongoClient.connect(
    "mongodb+srv://vashu:vashudev143@cluster0.sw0ls.mongodb.net/meetup?retryWrites=true&w=majority"
  );
  const db = client.db();
  const meetup = db.collection("meetup");
  const data = await meetup.find({}, { _id: 1 }).toArray();
  return {
    fallback: false,
    paths: data.map((data) => ({
      params: { meetupId: data._id.toString() },
    })),
  };
};
export const getStaticProps = async (context) => {
  const id = context.params.meetupId;
  const client = await MongoClient.connect(
    "mongodb+srv://vashu:vashudev143@cluster0.sw0ls.mongodb.net/meetup?retryWrites=true&w=majority"
  );
  const db = client.db();
  const meetup = db.collection("meetup");
  const data = await meetup.findOne({ _id: ObjectId(id) });
  return {
    props: {
      meetup: {
        id: data._id.toString(),
        title: data.title,
        image: data.image,
        address: data.address,
        description: data.description,
      },
    },
    revalidate: 1,
  };
};

export default MeetupDetails;
