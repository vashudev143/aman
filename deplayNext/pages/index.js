import { MongoClient } from "mongodb";
import MeetupList from "../components/meetups/MeetupList";
import Head from "next/head";
function HomePage(props) {
  return (
    <>
      <Head>
        <title>VASHU</title>
        <meta name="description" 
        content="Browse a list og highly meta data"
        />
      </Head>
      <MeetupList meetups={props.meetup} />
    </>
  );
}

export const getStaticProps = async () => {
  const client = await MongoClient.connect(
    "mongodb+srv://vashu:vashudev143@cluster0.sw0ls.mongodb.net/meetup?retryWrites=true&w=majority"
  );
  const db = client.db();
  const meetups = db.collection("meetup");
  const data = await meetups.find().toArray();
  client.close();

  return {
    props: {
      meetup: data.map((data) => ({
        title: data.title,
        id: data._id.toString(),
        address: data.address,
        image: data.image,
        description: data.description,
      })),
    },
    revalidate: 1,
  };
};

export default HomePage;
