import { useRouter } from "next/router";
import NewMeetupForm from "../../components/meetups/NewMeetupForm";
const NewMeetupPage = () => {
  const router = useRouter();
  const meetupHandler = async (data) => {
    const response = await fetch("/api/new-meetup", {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });
    const fetched = await response.json();
    router.replace("/");
  };
  return <NewMeetupForm onAddMeetup={meetupHandler} />;
};
export default NewMeetupPage;
