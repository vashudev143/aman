const MeetupDetail = (props) => {
  return (
    <div>
      <img src={props.img} />
      <div>{props.title}</div>
      <div>{props.address}</div>
    </div>
  );
};

export default MeetupDetail;
