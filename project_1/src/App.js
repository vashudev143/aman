import React from "react";
import ExpenseMain from "./components/ExpenseMain";
import ExpenseMainData from "./components/ExpenseMainData";

const App = () => {
  const run = [
    {
      id: 0,
      title: "car insurance",
      amount: 255.5,
      date: new Date(),
    },
    {
      id: 1,
      title: "car insurance",
      amount: 255.5,
      date: new Date(),
    },
    {
      id: 1,
      title: "not insurance",
      amount: 255.5,
      date: new Date(),
    },
    {
      id: 1,
      title: "car ",
      amount: 255.5,
      date: new Date(),
    },
  ];
  return (
    <>
      <ExpenseMainData item={run} />
    </>
  );
};

export default App;
