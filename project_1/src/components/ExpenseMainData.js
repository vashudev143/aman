import React from "react";
import ExpenseMain from "./ExpenseMain";
const ExpenseMainData = (props) => {
  return (
    <>
      {props?.item.map((expense, index) => {
        return (
          <ExpenseMain
            title={expense.title}
            amount={expense.amount}
            date={expense.date}
            key={index}
          />
        );
      })}
    </>
  );
};

export default ExpenseMainData;
