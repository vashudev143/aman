import React from "react";
import "./ExpenseMain.css";
const ExpenseMain = (props) => {
  return (
    <>
      <div className="main">
        <div className="main-2">
          <div className="date">{props.date.toLocaleString()}</div>
          <div className="car">{props.title}</div>
        </div>
        <div className="amount">{props.amount}</div>
      </div>
    </>
  );
};

export default ExpenseMain;
