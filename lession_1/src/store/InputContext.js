import { createContext, useState } from "react";

export const InputContext = createContext({
  data: [],
  getData: (inputData) => {},
});

const InputState = (props) => {
  const [data, setData] = useState([]);
  const getData = (inputData) => {
    return setData([...data, inputData]);
  };
  return (
    <InputContext.Provider value={{ data: data, getData: getData }}>
      {props.children}
    </InputContext.Provider>
  );
};
export default InputState;
