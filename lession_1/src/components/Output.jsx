import React, { useContext } from "react";
import { useLocation } from "react-router-dom";
import { InputContext } from "../store/InputContext";

const Output = () => {
  const location = useLocation();
  console.log(location.state);
  const { data } = useContext(InputContext);
  return (
    <div>
      {data.map((data, index) => {
        return (
          <li key={index}>
            {data.name} and {data.email}
          </li>
        );
      })}
    </div>
  );
};

export default Output;
