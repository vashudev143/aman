import React from "react";
import { useNavigate } from "react-router-dom";
const Home = () => {
  const navigate = useNavigate();
  const inp = () => {
    navigate("/inp");
  };
  const out = () => {
    navigate("/out");
  };

  return (
    <div>
      <ul style={{ cursor: "pointer" }}>
        <li onClick={inp}>Input</li>
        <li onClick={out}>Output</li>
      </ul>
    </div>
  );
};

export default Home;
