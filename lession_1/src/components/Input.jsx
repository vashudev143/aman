import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { InputContext } from "../store/InputContext";
const Input = () => {
  const { getData } = useContext(InputContext);
  const [inputData, setInputData] = useState({
    name: "",
    email: "",
  });
  const getDataa = (data) => {
    const { name, value } = data.target;
    setInputData({ ...inputData, [name]: value });
  };
  const navigate = useNavigate();
  const sendData = (event) => {
    event.preventDefault();
    getData(inputData);
    navigate("/out", { state: "vashu" });
  };

  return (
    <>
      <form onSubmit={sendData}>
        <input
          type="text"
          onChange={getDataa}
          value={inputData.name}
          name={"name"}
        />
        <input
          type="text"
          onChange={getDataa}
          value={inputData.email}
          name={"email"}
        />
        <button>Go</button>
      </form>
    </>
  );
};

export default Input;
