import { Routes, Route } from "react-router-dom";
import Home from "./components/Home";
import Input from "./components/Input";
import Output from "./components/Output";
import InputState from "./store/InputContext";
const App = () => {
  return (
    <InputState>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/inp" element={<Input />} />
        <Route path="/out" element={<Output />} />
      </Routes>
    </InputState>
  );
};

export default App;
